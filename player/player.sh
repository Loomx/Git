#!/bin/sh
#
# /usr/local/bin/player,sh
#
# Depends: mplayer, fzy, stest, inotifywait

# Music directory
Music=~/Music
cd "$Music" || exit 1

# Cache files:
AlbumCache="$Music"/.album_cache
TrackCache="$Music"/.track_cache

# Update caches if anything has changed:
if stest -dlq -n "$AlbumCache" "$Music"; then
    stest -dl "$Music" | sort >"$AlbumCache"
    stest -f */* >"$TrackCache"
fi

# Set trap to clean up afterwards:
clean_up () { rm -f /tmp/mp_output /tmp/status_msg; }
trap 'clean_up' EXIT

# Select an album:
Album="$( { printf "Jukebox\n"; cat "$AlbumCache"; } | fzy )" || exit

case "$Album" in
    Jukebox)
		read -p "Filters? " Filters
        if [ "$Filters" ]; then
            # Apply filters and shuffle tracks:
            set $( echo "$Filters" )
            for i; do 
                grep -Fi "$i" "$TrackCache"
            done | mplayer -playlist - -shuffle -vo null -identify >/tmp/mp_output &
        else
            # No filters, so shuffle all tracks:
            mplayer -playlist "$TrackCache" -shuffle -vo null -identify >/tmp/mp_output &
        fi
        ;;

    *)
        # Album is selected, prompt to play, shuffle or choose track:
        Track="$( { printf "Play\nShuffle\n"; stest -fl "$Music"/"$Album" | sort; } | fzy )" || exit

        case "$Track" in
            Play)
                mplayer "$Music"/"$Album"/* -vo null -identify >/tmp/mp_output &
                ;;

            Shuffle)
                mplayer "$Music"/"$Album"/* -shuffle -vo null -identify >/tmp/mp_output &
                ;;

            *)
                mplayer "$Music"/"$Album"/"$Track" -vo null -identify >/tmp/mp_output &
                ;;
        esac
        ;;
esac

# Pause while mplayer starts:
sleep 1

# Loop while playing to store current track name:
while pgrep mplayer >/dev/null; do
    tail -20 /tmp/mp_output | awk -F "/" '/FILENAME/ { print $NF }' >/tmp/status_msg
    inotifywait -q -e modify /tmp/mp_output >/dev/null
done
